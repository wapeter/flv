#ifndef _FLV_H_
#define _FLV_H_
#include <stdint.h>

// flv-format-format-spec.pdf  page: 74 (68)

// SWF file format, the byte sequence that represents the number 300 (0x12C) is 0x2C 0x01; as a UI16 in FLV file format, the byte sequence that represents the number 300 is 0x01 0x2C
// NOTE UB[5] means 5 bits

/*
Signature UI8 Signature UI8 Signature UI8 Version UI8 TypeFlagsReserved UB [5] TypeFlagsAudio UB [1] TypeFlagsReserved UB [1] TypeFlagsVideo UB [1] DataOffset UI32
Comment
UI8  Signature byte always 'F' (0x46)
UI8  Signature byte always 'L' (0x4C)
UI8  Signature byte always 'V' (0x56)
UI8  version: File version (for example, 0x01 for FLV version 1) Shall be 0
UB[5] TypeFlagsReserved : shall be 0 
UB[1] TypeFlagsAudio 1 = Audio tags are present
UB[1] TypeFlagsReserved : Shall be 0
UB[1] TypeFlagsVideo 1 = Video tags are present
UI32  DataOffset The length of this header in bytes
 */

#define FLV_HEADER_VIDEO_FLAG	(0x1)
#define FLV_HEADER_AUDIO_FLAG	(0x4)

#define FLV_SIGNATURE_SIZE	(3)

// peter: tested work in gcc in Mac Apple LLVM version 8.1.0 (clang-802.0.42)
// if we do not have this, we will end up sizeof(FLV_HEADER)=12
// after using pack(1), we have sizeof(FLV_HEADER)=9
#pragma pack(1)
typedef struct flv_header {
	uint8_t signature[FLV_SIGNATURE_SIZE];  // 'F', 'L', 'V'
	uint8_t version;  // 0 or 1 ?

	// UB[5]=reserved, UB[1]=audio_flag, UB[1]reserved2, UB[1]=video_flag
	uint8_t flag;
	uint32_t offset;
//	uint32_t offset; // length of this header in bytes
} FLV_HEADER;

#define FLV_TAG_AUDIO	(8)
#define FLV_TAG_VIDEO	(9)
#define FLV_TAG_SCRIPT	(18)


// this may be variable size 
#define FLV_TAG_TYPE(tag)	((tag)->flag & 0x1f) 
#define FLV_TAG_FILTER_MASK	(0x20)   
#define FLV_TAG_FILTER(tag)		(((tag)->flag & 0x20) >> 6)
// return 32 bits integer of UI24 24 bits integer uint8_t[3]

// this byte-order based on FLV spec
#define FLV_UINT24(data)	((((data[0]) & 0xff) << 16) | (((data[1]) & 0xff) << 8) |  ((data[2]) & 0xff))
#define FLV_TAG_DATA_SIZE(tag)	FLV_UINT24((tag)->data_size)

#pragma pack(1)
typedef struct flv_tag {
	uint8_t flag;	// UB[2]=reserved  UB[1]=filter  UB[5]=tagtype
	// tagtype:
	// 8=audio
	// 9=video
	// 18=script data
	uint8_t data_size[3];	// 24 bits unsigned int, strange
	uint8_t timestamp[3];	// millisecond (24 bits)
	uint8_t timestamp_extended;
	uint8_t stream_id[3];	// always 0

	// TODO
	// if tagtype=audio (8) then FLV_AUDIO_TAG_HEADER
	// if tagtype=video (9) then FLV_VIDEO_TAG_HEADER
	// if filter=1 then ENCRYPTED_HEADER
	// if filter=1 then FILTER_PARAMS
	// DATA:  AUDIO / VIDEO / SCRIPT_DATA

} FLV_TAG;


/* script data value
Type UI8
0 = Number   (double
1 = Boolean  (UI8)
2 = String
3 = Object
4 = MovieClip (reserved, not supported) 
5 = Null
6 = Undefined
7 = Reference
8 = ECMA array
9 = Object end marker 10 = Strict array
11 = Date
12 = Long string
*/


#define SCRIPT_NUMBER	(0)
#define SCRIPT_BOOLEAN	(1)
#define SCRIPT_STRING	(2)
#define SCRIPT_OBJECT	(3) // peter: will not support ?
#define SCRIPT_MOVIECLIP	(4)	// not support?
#define SCRIPT_NULL		(5)	// 1 byte?
#define SCRIPT_UNDEFINED	(6)
#define SCRIPT_REFERENCE	(7)
#define SCRIPT_ECMA_ARRAY	(8)
#define SCRIPT_OBJECT_END_MARKER	(9)

#define SCRIPT_STRICT_ARRAY		(10)
#define SCRIPT_DATA			(11)
#define SCRIPT_LONG_STRING	(12)


#define MID_STRING	(1024)


#define FLV_SOUNDFORMAT_PCM	(0)
#define FLV_SOUNDFORMAT_ADPCM (1)
#define FLV_SOUNDFORMAT_MP3 (2)
// ...
#define FLV_SOUNDFORMAT_ALAW (7)
#define FLV_SOUNDFORMAT_MULAW (8)
#define FLV_SOUNDFORMAT_RESERVED (9)
#define FLV_SOUNDFORMAT_AAC (10)
#define FLV_SOUNDFORMAT_SPEEX (11)
#define FLV_SOUNDFORMAT_MP3_8KHZ (14)
#define FLV_SOUNDFORMAT_DEVICE_SPECIFIC (15)
// NOTE from spec Formats 7, 8, 14, and 15 are reserved.
// AAC is supported in Flash Player 9,0,115,0 and higher. 
// Speex is supported in Flash Player 10 and higher.


#define FLV_SOUNDRATE_5DOT5K	(0)	// 5.5K
#define FLV_SOUNDRATE_11K	(1)
#define FLV_SOUNDRATE_22K	(2)
#define FLV_SOUNDRATE_44K	(3)	// for AAC always 3

#define FLV_AUDIO_SOUNDFORMAT(flag)	(((flag) >> 4) & 0xf)
#define FLV_AUDIO_SOUNDRATE(flag)	(((flag)>>2) & 0x3)
#define FLV_AUDIO_SOUNDSIZE(flag)	(((flag)>>1) & 0x1)
#define FLV_AUDIO_SOUNDTYPE(flag)	((flag) & 0x1)  // 0=mono, 1=stereo, AAC=1

// not used
#pragma pack(1)
typedef struct flv_audio {
	uint8_t flag;
} FLV_AUDIO;
// AACPacketType UI8  if soundformat=10  (0=aac sequence header, 1=aac raw)

// UB[4] frametype :
	// 1=key frame
	// 2=interframe
	// 3=disposable interframe
	// 4=generated key frame
	// 5=video info/command frame

// UB[4] codeid :
//   1=jpeg_unused
//   2=sorenson H.263
//   3=screen video
//   4=on2 vp6
//   5=on2 vp6 with alpha channel
//   6=screen video version2 
//   7=AVC
#define FLV_VIDEO_FRAMETYPE(flag)	(((flag)>>4) & 0xf)
#define FLV_VIDEO_CODEID(flag)	((flag) & 0xf)

// omit the suffix FRAME, e.g. KEY = KEY_FRAME
#define FLV_FRAMETYPE_KEY		(1)
#define FLV_FRAMETYPE_INTER		(2)
#define FLV_FRAMETYPE_DISPOSABLE_INTER	(3)
#define FLV_FRAMETYPE_GENERATED_KEY		(4)
#define FLV_FRAMETYPE_COMMAND		(5)


//   1=JPEG (currently unused) ?
//   2=sorenson H.263
//   3=screen video
//   4=on2 vp6
//   5=on2 vp6 with alpha channel
//   6=screen video version2 
//   7=AVC



// ref: https://blog.csdn.net/spygg/article/details/53896179
// bitwise declaration

#pragma pack(1)
typedef struct flv_audio_info
{
	uint8_t audio_type:1;
	uint8_t size:1;
	uint8_t rate:2;
	uint8_t audio_format:4;
} FLV_AUDIO_INFO;


#pragma pack(1)
typedef struct flv_video_info
{
	uint8_t code_id:4;
	uint8_t frame_type:4;
} FLV_VIDEO_INFO;


// function prototype
int print_flv_header(FLV_HEADER * header, const char *label);
int get_flv_header(FLV_HEADER *header, uint8_t *ptr);
int get_flv_tag(uint8_t *ptr);
int get_uint32(uint32_t *output, uint8_t *data);


#endif  // _FLV_H_
