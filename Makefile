
all:	flv test_header

clean:	
	rm flv
	rm tags



flv:	flv.c flv.h
	gcc -DCOLOR_TAG -o flv flv.c


test_header:	flv.c flv.h test_header.c
	gcc -DCOLOR_TAG -DFLV_LIB -o test_header test_header.c flv.c 

