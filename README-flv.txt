peterwork: ~/Documents/workspace/flv/README-flv.txt

git clone git@bitbucket.org:wapeter/flv.git

flv1 problem

## (focus 66 firmware 01.19.44)
## problem flv1

## this is bad.flv : was 000AE21F1525_04_20180901061111000.flv
ffprobe bad.flv
## Output last 2 lines: 
Stream #0:0: Audio: mp3, 44100 Hz, stereo, s16p, 128 kb/s
Stream #0:1: Video: flv1, yuv420p, 400x300, 200 kb/s, 30 fps, 30 tbr, 1k tbn


## this is a good.flv: was 000AE21F1525_04_20180902201111000.flv
ffprobe good.flv
## Output last 2 lines:
Stream #0:0: Video: h264 (Baseline), yuv420p(progressive), 1280x720, 15 fps, 1k tbr, 1k tbn
Stream #0:1: Audio: pcm_alaw, 8000 Hz, mono, s16, 64 kb/s
## peter: notice the Stream #0:0 is video, Stream #0:1 is audio

## using ffmpeg to check:  (bad one)
ffmpeg -i bad.flv -vcodec copy -acodec aac output.mp4
##
## error message
[mp4 @ 0x7ff73f000000] Could not find tag for codec flv1 in stream #0, codec not currently supported in container



For Mac user:
brew install ffmpeg
## this will include the ffprobe
brew install mediainfo
## mediainfo is a replacement for ffprobe if that one not working.

For Ubuntu Linux :
apt-get install ffmpeg
apt-get install mediainfo

