/**
 * flv.c for reading and parsing flv file.
 * usage: flv [filename.flv] [epoch]
 * default filename="good.flv"   epoch=10
 * epoch is the max number of tags(chunks) to be read, you can 
 * specify a very large number to read the whole file (e.g. epoch=1000000).
 * 
 * ERROR code follows Tam's error code of conduct.
 * key functions:
 * read_flv(const char *filename, int epoch)
 * - read_flv_header : 11 bytes flv header
 * - read_flv_tag
 * -- get_script_data : get the meta info, using get_data_value and
 *    get_data_xxx functions (get_data_double, get_data_string etc)
 *
 * for printing function, all start with prefix print_xxx,
 * e.g. print_flv_header, print_flv_tag, print_flv_video, print_flv_audio
 * all printing functions come with a "label", which is used by the caller
 * to specify a label for printing, as a way to tell user where it comes from.
 *
 * naming convention:
 * - read_xxx  : read from a file description, int fd, e.g. read_uint8, 
 *   read_uint32, 
 * - get_xxx : get data from a uint8_t *buffer, print out or return something
 *   by pointer
 * - const char * xxx_str(int value)  : for integer constant to a constant string
i*   for a human readable form, e.g. frametype_str, soundformat_str, codeid_str
 *   (for video codec id)
 * 
 * reference:
 * mydoc/book/h263-T-REC-H263-200501.pdf
 * mydoc/book/h264-T-REC-H264-201704.pdf
 * mydoc/book/flv-format-spec.pdf
 * mydoc/book/mp4-iso14496-12.pdf
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <arpa/inet.h>	// for ntohl
#include "fatal.h"
#include "flv.h"

///// START utility
// str must have at least 9 bytes (8 bits + '\0')
char * bit_string(char *str, uint8_t bit)
{
	str[0] = '0' + ((bit>>7) & 1);
	str[1] = '0' + ((bit>>6) & 1);
	str[2] = '0' + ((bit>>5) & 1);
	str[3] = '0' + ((bit>>4) & 1);
	str[4] = '0' + ((bit>>3) & 1);
	str[5] = '0' + ((bit>>2) & 1);
	str[6] = '0' + ((bit>>1) & 1);
	str[7] = '0' + ((bit) & 1);
	str[8] = '\0'; // null terminator
	return str;
}
///// END utility


///// START io utility no dependency on other io utility
int read_data(int fd, uint8_t *data, uint32_t size)
{
    int len = size;
    int ret;
    int epoch = 500; // more for larger size
    int offset = 0;
    do {
        ret = read(fd, data+offset, len);
        if (ret <= 0) {
            if (ret == 0) { ret = -5; }  // I/O, end of file
			// offset=0 is normal, if offset>0 something is read but not all
            ERROR_RETURN(ret, "read_data: read len=%d  offset=%d", len, offset);
            return ret;
        }
        len -= ret;
        offset += ret;
        // printf("read_data: ret=%d  len=%d  offset=%d\n", ret, len, offset);
    } while (len > 0 && --epoch>0) ;

    return offset;
}


int read_uint8(int fd, uint8_t *value)
{
    int len = 1;
    int ret;
    int epoch = 100; // 100 retry
    int offset = 0;

    do {
        ret = read(fd, value, len);
        if (ret <= 0) {
            if (ret == 0) { ret = -5; }  // I/O, end of file
            ERROR_RETURN(ret, "read_uint8: read len=%d", len);
            return ret;
        }
        len -= ret;
        offset += ret;
    } while (len > 0 && --epoch>0) ;

    return offset;
}

int read_uint32(int fd, uint32_t *value)
{
    uint8_t buffer[4] = {0};
    int len = 4;
    int ret;
    int epoch = 100; // 100 retry
    int offset = 0;

    do {
        ret = read(fd, buffer+offset, len);
        if (ret <= 0) {
            if (ret == 0) { ret = -5; }  // I/O, end of file
            ERROR_RETURN(ret, "read_uint32: read len=%d", len);
            return ret;
        }
        len -= ret;
        offset += ret;
    } while (len > 0 && --epoch>0) ;

	if (epoch <= 0 && offset != 4) {
		ERROR_RETURN(-15, "read_uint32: epoch overflow offset=%d", offset);
	}

    *value = ntohl(*((uint32_t*)buffer));
    return offset;
}

///////// END io utility


///////// START flv reading and parsing and printing functions

// this one can move to flv.h or some common.h
typedef struct _value_str {
	int value;
	const char * str;
} VALUE_STR;


const char * tag_str(int value)
{
	static const VALUE_STR value_str[] = {
		{FLV_TAG_AUDIO, "TAG_AUDIO"}
		, {FLV_TAG_VIDEO, "TAG_VIDEO"}
		, {FLV_TAG_SCRIPT, "TAG_SCRIPT"}
	};

	int size = sizeof(value_str) / sizeof(VALUE_STR);
	for (int i=0; i<size; i++) {
		if (value==value_str[i].value) {
			return value_str[i].str;
		}
	}
	return "unknown";
}


// TODO soundrate, soundtype  VALUE_STR?
const char * soundformat_str(int value)
{
	static const VALUE_STR soundformat_string[] = {
		  {FLV_SOUNDFORMAT_PCM, "pcm"}
		, {FLV_SOUNDFORMAT_ADPCM, "adpcm"}
		, {FLV_SOUNDFORMAT_MP3, "mp3"}
		, {FLV_SOUNDFORMAT_ALAW, "alaw"}
		, {FLV_SOUNDFORMAT_MULAW, "mulaw"}
		, {FLV_SOUNDFORMAT_RESERVED, "reserved"}
		, {FLV_SOUNDFORMAT_AAC, "aac"}
		, {FLV_SOUNDFORMAT_SPEEX, "speex"}
		, {FLV_SOUNDFORMAT_MP3_8KHZ, "mp3_8khz"}
		, {FLV_SOUNDFORMAT_DEVICE_SPECIFIC, "device_specific"}
	};

	int size = sizeof(soundformat_string) / sizeof(VALUE_STR);
	for (int i=0; i<size; i++) {
		if (value==soundformat_string[i].value) {
			return soundformat_string[i].str;
		}
	}

	WARN_PRINT(-2, "unknown soundformat %d", value);
	return "unknown";
}


const char * frametype_str(int value)
{
	static const VALUE_STR value_str[] = {
  		  {FLV_FRAMETYPE_KEY, "key"}
		, {FLV_FRAMETYPE_INTER, "inter"}
		, {FLV_FRAMETYPE_DISPOSABLE_INTER, "disposable_inter"}
		, {FLV_FRAMETYPE_GENERATED_KEY, "generated_key"}
		, {FLV_FRAMETYPE_COMMAND, "command"}
	};

	static int size = sizeof(value_str) / sizeof(VALUE_STR);
	for (int i=0; i<size; i++) {
		if (value==value_str[i].value) {
			return value_str[i].str;
		}
	}
	WARN_PRINT(-2, "unknown frame type %d", value);
	return "unknown";
}


const char * codeid_str(int value)
{
	static const VALUE_STR value_str[] = {
  		  {1, "jpeg_unused"}
  		, {2, "sorenson_h263"}
		, {3, "screen_video"}
		, {4, "on2_vp6"}
		, {5, "on2_vp6_with_alpha"}
		, {7, "avc_h264"}
	};

	static int size = sizeof(value_str) / sizeof(VALUE_STR);
	for (int i=0; i<size; i++) {
		if (value==value_str[i].value) {
			return value_str[i].str;
		}
	}

	WARN_PRINT(-2, "unknown audio codec %d", value);
	return "unknown";
}


void print_hex(uint8_t *data, size_t size)
{
	for (int i=0; i<size; i++) {
		printf("  %02x", data[i]);
	}
	printf("\n");
}


// offset : 0x1001200
int print_flv_header(FLV_HEADER * header, const char *label)
{
	char str[10];
	if (label==NULL) label = "flv_header";

	printf("%s: signature[3]=%c%c%c  version=%d  flagUB[8]=%s  audio_flag=%d  video_flag=%d   offset=%d\n",
	label, header->signature[0], header->signature[1], header->signature[2],
	(int)header->version, bit_string(str, header->flag),
	header->flag & FLV_HEADER_AUDIO_FLAG, header->flag & FLV_HEADER_VIDEO_FLAG,
	(int)header->offset
	);

	return 0;
}


int print_flv_tag(FLV_TAG *tag, const char *label)
{
	char str[10];
	if (label==NULL) label = "flv_tag";

	printf("%s: (hex) ", label);
	print_hex((uint8_t *) tag, sizeof(FLV_TAG));
	// note: timestamp in ms millisecond
	printf("%s: tag=%s(%d)  flagUB[8]=%s  filter=%d  data_size=%d  timestamp=%d"
	"  timestamp_extend=%d  stream_id=%d\n",
		label,  tag_str(FLV_TAG_TYPE(tag)), FLV_TAG_TYPE(tag), 
		bit_string(str, tag->flag), 
		FLV_TAG_FILTER(tag),
		// FLV_TAG_DATA_SIZE(tag),
		FLV_UINT24(tag->data_size),
		FLV_UINT24(tag->timestamp), (int)tag->timestamp_extended,
		FLV_UINT24(tag->stream_id)
	);
	return 0;
}


// flv_audio: flag=00101111  soundformat=15  soundrate=2  soundsize=0  soundtype=0
// soundformat=15 : device-specific sound
int print_flv_audio(uint8_t flag, const char *label)
{
	char str[10];
	if (NULL==label) label = "flv_audio";
	printf("%s: flag=%s  soundformat=%d(%s)  soundrate=%d  soundsize=%d  soundtype=%d\n",
	label, bit_string(str, flag), FLV_AUDIO_SOUNDFORMAT(flag), 
	soundformat_str( FLV_AUDIO_SOUNDFORMAT(flag)),
	FLV_AUDIO_SOUNDRATE(flag), FLV_AUDIO_SOUNDSIZE(flag),
	FLV_AUDIO_SOUNDTYPE(flag)
	);
	return 0;
}


int print_audio_info(FLV_AUDIO_INFO *info, const char *label)
{
	if (NULL==label) label = "audio_info";
	printf("%s:  format=%d  rate=%d  size=%d  type=%d\n",
		label, info->audio_format, info->rate, info->size, info->audio_type
	);
	return 0;
}


int print_flv_video(uint8_t flag, const char *label)
{
	char str[10];
	if (NULL==label) label="flv_video";
	int frametype = FLV_VIDEO_FRAMETYPE(flag);
	int codeid = FLV_VIDEO_CODEID(flag);
	printf("%s: flag=%x(%s)  frametype=%d(%s)  codeid=%d(%s)\n",
		label, 0xff & flag, bit_string(str, flag),
		frametype, frametype_str(frametype),
		codeid, codeid_str(codeid)
	);
	return 0;
}


// FLV_VIDEO_INFO is the flag in bit-form
int print_video_info(FLV_VIDEO_INFO *info, const char *label)
{
	char str[10];
	if (NULL==label) label = "video_info";
	printf("%s:  frametype=%d(%s)  codeid=%d(%s)\n",
		label, 
		info->frame_type, frametype_str(info->frame_type),
		info->code_id, codeid_str(info->code_id)
	);

	return 0;
}


// THE ONE AND ONLY ONE FORWARD DECLARATION, avoid this at all cost (cannot)
// TODO rename to flv specific: this is get_flv_data_value
int get_data_value(uint8_t *data, int size); // forward declaration

// copy the data to output, simply an alias of memcpy
int get_data(uint8_t *output, uint8_t *data, uint32_t size)
{
    memcpy(output, data, size);
	return size;
}


// intel specific
int get_uint16(uint16_t *output, uint8_t *data)
{
	*output = ((data[0] & 0xff) << 8) | (data[1] & 0xff);	 
	return 2;
}


// intel specific?  should be generic ok
int get_uint32(uint32_t *output, uint8_t *data)
{
	*output = ((data[0] & 0xff) << 24) | ((data[1] & 0xff) << 16) 
	| ((data[2] & 0xff) << 8) |	 (data[3] & 0xff);	 
	return 4;
}


// 3 bytes 24 bits to a normal integer
// see FLV_UINT24 in flv.h
int get_uint24(uint32_t *output, uint8_t *data)
{
	*output = ((data[0] & 0xff) << 16) | ((data[1] & 0xff) << 8) 
	| (data[2] & 0xff);
	return 3;
}


int get_data_string(uint16_t *len, char *str, uint8_t *data, int size)
{
	uint8_t *start = data;
	
	*len = ((data[0] & 0xff) << 16) | (data[1] & 0xff);	 data+=2;
	if (*len >= MID_STRING) {
		ERROR_RETURN(-2, "get_data_string:str len too long %d", *len);
	}
	memcpy(str, data, *len);	 data += *len;
	str[*len] = '\0';
	
	return data-start;
}


#define SIZEOF_DOUBLE	(8)
int get_data_double(double *output, uint8_t *data)
{
	uint8_t *ptr=(uint8_t*)output;
	// C99 stanard: IEC 60559 floating-point arithmetic  double is 8 bytes
	// adobe use reverse order?

	ptr[0] = data[7];
	ptr[1] = data[6];
	ptr[2] = data[5];
	ptr[3] = data[4];
	ptr[4] = data[3];
	ptr[5] = data[2];
	ptr[6] = data[1];
	ptr[7] = data[0];

//	for (int i=0; i<SIZEOF_DOUBLE; i++) { // sizeof(double) = 8 expected
//		ptr[i] = data[i];
//	}
	return SIZEOF_DOUBLE;
}


int get_ecma_array(uint8_t *data, int size)
{
	uint8_t *start = data;
	uint32_t len = 0;
	char property_name[MID_STRING+1] = {0};
	uint16_t str_len = 0;
	int ret;
	// len  UI32 : number of items in ECMA array
	// SCRIPT_DATA_OBJECT_PROPERTY[] : list of variable names and values
	// SCRIPT_DATA_OBJECT_END  (3 bytes: UI8[3] = 0, 0, 9
	data += get_uint32(&len, data);

	printf("get_ecma_array array len=%d\n", len);

	for (int i=0; i<len; i++) {
// int get_data_string(uint16_t *len, char *str, uint8_t *data, int size)
		// TODO use size - xxx to reduce risk of buffer overflow
		ret = get_data_string(&str_len, property_name, data, size);
		ERROR_NEG_RETURN(ret, "get_ecma_array:get_data_string %d", i);
		printf("get_ecma_array[%d].property_name(%d)=%s\n",i, (int)str_len, property_name);
		data += ret;

		ret = get_data_value(data, size);
		ERROR_NEG_RETURN(ret, "get_ecma_array:get_data_string %d", i);
		data += ret;
	}


	// get data object end 
	// 0, 0, 9
	int object_end = (data[0]==0 && data[1]==0 && data[2]==9);
	printf("get_ecma_array object_end=%d   %02x %02x %02x\n", object_end, 0xff & data[0], 0xff & data[1], 0xff & data[2]);

	if (0==object_end) {
		ERROR_RETURN(-6, "ecma_array:no object_end %02X %02X %02X (expect 0,0,9)",
		0xff & data[0], 0xff & data[1], 0xff & data[2]);
	}
	data += 3; // object end length (0, 0, 9)

	return data - start;
}


// peter: beware of recursive stack overflow
int get_data_value(uint8_t *data, int size)
{
	uint8_t *start = data;
	char str[MID_STRING+1] = {0};
	uint16_t str_len = 0;
	uint8_t data_type;
	uint8_t b; // boolean
	double d;
	int ret;

	data_type = *data;	data++;
	switch (data_type) {
		case SCRIPT_NUMBER:
		data += get_data_double(&d, data);
		printf("data double=%lf\n", d);
		break;

		case SCRIPT_BOOLEAN:
		b = *data;  data++;
		printf("data boolean=%d\n", (int)b);
		break;

		case SCRIPT_STRING:
		ret = get_data_string(&str_len, str, data, size);
		ERROR_NEG_RETURN(ret, "get_data_value:string len error %d", (int) str_len);
		printf("data str(%d)=%s\n", (int)str_len, str);
		data += ret;
		break;

		case SCRIPT_ECMA_ARRAY:
		ret = get_ecma_array(data, size);
		ERROR_NEG_RETURN(ret, "get_data_value:get_ecma_array");
		data += ret;
		break;


		default:
		ERROR_RETURN(-10, "get_data_value: script data type not support %d\n", 
			data_type);
		return -10;
	}

	return data-start;
}



/**

NAME=onMetaData
VALUE=ecma array

audiocodecid NUMBER : audio codec ID see E.4.2.1 for available SoundFormat
audiodatarate NUMBER : audio bit rate in kilobits per second
audiodelay NUMBER : delay introduced by the audio codec in seconds
audiosamplerate NUMBER: frequency at which the audio stream is replayed
audiosamplesize NUMBER : resolution of a single audio sample
canSeekToEnd BOOLEAN : indicate the last video frame is a key frame
creationdate STRING : creation date and time
duration NUMBER : total duration of the file in seconds
filesize NUMBER : total size of the file in bytes
framerate NUMBER : number of frames per second
height NUMBER : height of video in pixels
stereo BOOLEAN : indicating stereo audio
videocodecid NUMBER : video codec ID used in the file see E.4.3.1
videodatarate NUMBER : video bit rate in kilobits per second
width NUMBER : width of video in pixels
*/
int get_script_data(uint8_t *data, int size)
{
	// name value pair
	uint8_t *start = data;
	int start_size = size;
	const int epoch = 300; // max 300 entries
	int ret;
	int remain;

	printf("----- START script data -----\n");
	
	// TODO for loop
	// NAME and VALUE only
	// printf("> NAME:\n");
	ret = get_data_value(data, size); // TODO need restrict size later
	ERROR_NEG_RETURN(ret, "get_script_data NAME");
	data += ret;  
	// printf("> VALUE:\n");
	ret = get_data_value(data, size); // TODO need restrict size later
	ERROR_NEG_RETURN(ret, "get_script_data VALUE");
	data += ret;  

	remain = data - start - size;
	printf("----- END script data used=%d  size=%d  remain=%d -----\n", 
	(int)(data-start), start_size, remain);

	// non-zero will produce warning
	WARN_PRINT(remain, "get_script_data:remain!=0 %d", remain);
	return data - start;
}


int read_flv_audio(int fd, int size)
{
	int offset = 0;
	uint8_t *buffer;
	uint8_t flag;
	int ret;

	printf("read_flv_audio:size=%d\n", size);

	ret = read_uint8(fd, &flag);	// may do: read_uint8(fd, (uint8_t)&info);
	ERROR_NEG_RETURN(ret, "read_flv_audio:read_flag");
	offset += ret;
	// FLV_AUDIO_INFO is a bit-wise structure for flag
	// TODO check whether all compiler support bit-wise
	FLV_AUDIO_INFO info = *((FLV_AUDIO_INFO*)&flag);	// another way of implementation
	print_flv_audio(flag, "AUDIO");
	// printf("sizeof(FLV_AUDIO_INFO)=%zu\n", sizeof(FLV_AUDIO_INFO));
	// print_audio_info(&info, "AUDIO_INFO");


	// TODO if soundformat==10 read one byte AACPacketType

	buffer = malloc(size);  // can be size-offset
	ret = read_data(fd, buffer, size-offset);
	if (ret < 0) {
		free(buffer);
		ERROR_NEG_RETURN(ret, "read_flv_audio:read_data");
	}
	offset += ret;
	// why we are not using the buffer?  it is for testing only

	int remain = size-offset;
	printf("read_flv_audio:remain=%d\n", remain);
	WARN_PRINT(remain, "read_flv_audio:remain>0");

	free(buffer); // cleanup
	return offset;
}


// video tag E4.3
int read_flv_video(int fd, int size)
{
	uint8_t flag = 0 ;
	int offset = 0;
	int ret = 0;
	uint8_t *buffer;

	// ret = read_uint8(fd, &flag);
    ret = read(fd, &flag, 1);
	ERROR_NEG_RETURN(ret, "read_flv_vide:read_uint8 flag");
	offset += ret;

	// TODO if code_id==7 :
	// read UI8
	// read SI24  (24bits signed integer?)

	print_flv_video(flag, "VIDEO");
	FLV_VIDEO_INFO info = *(FLV_VIDEO_INFO *)&flag;
	// print_video_info(&info, "VIDEO_FLAG");

	buffer = malloc(size-offset);
	ret = read_data(fd, buffer, size-offset);
	if (ret < 0) {
		free(buffer);
		ERROR_NEG_RETURN(ret, "read_flv_video:read_data");
	}
	offset += ret;

	int remain = size - offset;
	// printf("read_flv_video:remain=%d\n", remain);
	WARN_PRINT(remain, "read_flv_video:remain>0");
	free(buffer);
	
	return offset;
}

int get_flv_tag(uint8_t *ptr)
{
	int ret;
	uint8_t *start = ptr;
	FLV_TAG tag = {0};
	int tag_type = 0;
	uint32_t data_size = 0;

	ret = get_data((uint8_t *) &tag, ptr, sizeof(FLV_TAG));
	ERROR_NEG_RETURN(ret, "get_flv_tag:read_data 111");
	ptr += ret;  // return is the offset
	print_flv_tag(&tag, "get_flv_tag");

	// TODO if tag
	tag_type = FLV_TAG_TYPE(&tag);
	data_size = FLV_TAG_DATA_SIZE(&tag);
	// DEBUG_PRINT(1, "get_flv_tag:check data_size=%d", data_size);

	switch (tag_type) {
		case FLV_TAG_AUDIO:
		// get_flv_audio : not yet implement
		ERROR_NEG_RETURN(-10, "get_flv_tag:get_flv_audio not yet implement");
		break;
		// ERROR_RETURN(-10, "tag_data:FLV_TAG_AUDIO not yet implement");

		case FLV_TAG_VIDEO:
		// HERE  TODO fix this ?
		// ret = get_flv_video(fd, data_size);
		ERROR_NEG_RETURN(-10, "get_flv_tag:get_flv_video not yet implement");
		break;
	}

	if (FLV_TAG_FILTER(&tag)) {
		// TODO read encrypt header
		// TODO read FILTER_PARAMS
		ERROR_RETURN(-10, "get_flv_tag:filter not yet implement");
	}

	// ok, we read script data if tag_type is FLV_TAG_SCRIPT
	if (FLV_TAG_SCRIPT==tag_type) {

		// TODO need to fill in FLV_META_DATA 
		ret = get_script_data(ptr, data_size);
		ERROR_NEG_RETURN(ret, "get_flv_tag:get_script_data");
		ptr += ret;
	}

	ret = ptr - start; // total bytes used
	return ret;
}


int read_flv_tag(int fd)
{
	int ret;
	FLV_TAG tag = {0};
	int tag_type = 0;
	uint32_t data_size = 0;

	ret = read_data(fd, (uint8_t *) &tag, sizeof(FLV_TAG));
	ERROR_NEG_RETURN(ret, "read_flv_tag:read_data 111");

	print_flv_tag(&tag, "read_flv_tag");


	// TODO
	// if tagtype=audio (8) then FLV_AUDIO_TAG_HEADER
	// if tagtype=video (9) then FLV_VIDEO_TAG_HEADER
	// if filter=1 then ENCRYPTED_HEADER
	// if filter=1 then FILTER_PARAMS
	// DATA:  AUDIO / VIDEO / SCRIPT_DATA

	// read the tag extension data

	// TODO if tag
	tag_type = FLV_TAG_TYPE(&tag);
	data_size = FLV_TAG_DATA_SIZE(&tag);
	// DEBUG_PRINT(1, "tag_data:check data_size=%d", data_size);
	switch (tag_type) {
		case FLV_TAG_AUDIO:
		ret = read_flv_audio(fd, data_size);
		ERROR_NEG_RETURN(ret, "tag_data:read_flv_audio");
		break;
		// ERROR_RETURN(-10, "tag_data:FLV_TAG_AUDIO not yet implement");

		case FLV_TAG_VIDEO:
		// HERE  TODO fix this ?
		ret = read_flv_video(fd, data_size);
		ERROR_NEG_RETURN(ret, "tag_data:read_flv_video");
		break;
	}

	if (FLV_TAG_FILTER(&tag)) {
		// TODO read encrypt header
		// TODO read FILTER_PARAMS
		ERROR_RETURN(-10, "tag_data:filter not yet implement");
	}

	// ok, we read script data if tag_type is FLV_TAG_SCRIPT
	if (FLV_TAG_SCRIPT==tag_type) {
		uint8_t *buffer;	// need free

		buffer = malloc(data_size);

		ret = read_data(fd, buffer, data_size);
		if (ret < 0) {
			free(buffer);
			ERROR_RETURN(ret, "tag_data:script read_data data_size=%d", data_size);
		}

		// TODO need to fill in FLV_META_DATA 
		ret = get_script_data(buffer, data_size);
		free(buffer);
		ERROR_NEG_RETURN(ret, "read_flv_tag:get_script_data");
	}

	// TODO not yet ready
	return 0;
}

// pointer version of read_flv_header, return the offset if normal
int get_flv_header(FLV_HEADER *pheader, uint8_t *ptr)
{
	int ret = -10;

	if (NULL==pheader) {
		ERROR_NEG_RETURN(-3, "get_flv_header:null pheader");
	}

	
	// signature(3 bytes) + version(1 byte) + flag(1 byte) 
	ret = get_data((uint8_t *) pheader, ptr, FLV_SIGNATURE_SIZE + 1 + 1);
	ptr += ret;

	// uint32 will do conversion on endian 
	ret = get_uint32(&pheader->offset, ptr);
	ptr += ret;

	ret = sizeof(FLV_HEADER);

	return ret;
}

int read_flv_header(int fd)
{
	int ret;
	FLV_HEADER header;
	uint8_t buffer[sizeof(FLV_HEADER)];

	ret = read_data(fd, buffer, sizeof(FLV_HEADER));
	ERROR_NEG_RETURN(ret, "read_flv_header:read_data");

	ret = get_flv_header(&header, buffer);
	ERROR_NEG_RETURN(ret, "read_flv_header:get_flv_header");

	print_flv_header(&header, "read_flv_header");

	return ret;
}


// TODO argv
int read_flv(const char * const filename, int epoch)
{
	int fd;
	int ret;
	uint32_t size;
	uint32_t file_size, total_read;
	struct stat stat;


	printf("---> START read_flv on filename=%s  epoch(max_tag)=%d\n",
		filename, epoch);

	fd = open(filename, O_RDONLY);
	ERROR_NEG_RETURN(fd, "open: %s", filename);

	ret = fstat(fd, &stat);
	ERROR_NEG_RETURN(ret, "fstat: %s", filename);
	file_size = stat.st_size;

	ret = read_flv_header(fd);
	ERROR_NEG_RETURN(ret, "read_flv_header");
	total_read = ret;  // include the header size

	// read the prev_tag_size
	size=0;


	for (int i=0; i<epoch; i++) {
		printf("====== START TAG %d =========\n", i);
		ret = read_uint32(fd, &size);
		if (ret < 0) {
			ERROR_NEG_PRINT(ret, "read_uint32:prev_tag_size [%d]", i);
			break;
		}
		total_read += size + 4;  // 4 for the tag size
		printf("+++ file_size=%d  total_read=%d  prev_tag_size=%d\n", file_size, total_read, size);

		ret = read_flv_tag(fd);
		if (ret < 0) {
			ERROR_NEG_PRINT(ret, "read_uint32:read_flv_tag [%d]", i);
			break;
		}
		printf("====== END TAG %d =========\n", i);
	}

	close(fd);	 // clean up
	printf("nice close:  for full read, please check file_size %d==total_read %d\n",
		file_size, total_read);

	return 0;
}


int test_uint24(int argc, const char *argv[])
{
	int verbose = (argc>2  && 0==strncmp(argv[2], "-v", 2));
	printf("verbose=%d\n", verbose);
	uint8_t data[3];
	uint32_t value1, value2;

	data[0] = 0xF5;
	data[1] = 0x0a;
	data[2] = 0xcc;

	// test whether FLV_UINT24 is the same as get_uint24
	value1 = FLV_UINT24(data);
	get_uint24(&value2, data);
	if (value1!=value2) {
		ERROR_NEG_RETURN(-6, "test_uint24: not match value1=0x%x value2=0x%x", 
		value1, value2);
	}

	if (verbose) INFO_PRINT(1, "test_uint24: value1=0x%x value2=0x%x", 
		value1, value2);
	return 0;
}


int test_audio_info(uint8_t flag)
{
	int ret;
	char str[10] = {0};
	FLV_AUDIO_INFO audio_info = *(FLV_AUDIO_INFO *) (&flag);

//	flag = flag ^ 0x2;	// negative test case

	ret = audio_info.audio_type != FLV_AUDIO_SOUNDTYPE(flag);
	ERROR_RETURN(ret, "test_bit_struct:audio_type not match audio_info.audio_type=%d  FLV_AUDIO_SOUNDTYPE=%d  bit=%s", audio_info.audio_type, FLV_AUDIO_SOUNDTYPE(flag), bit_string(str, flag));
	
	ret = audio_info.size != FLV_AUDIO_SOUNDSIZE(flag);
	ERROR_RETURN(ret, "test_bit_struct:size not match audio_info.size=%d  FLV_AUDIO_SOUNDSIZE=%d  bit=%s", audio_info.size, FLV_AUDIO_SOUNDSIZE(flag), bit_string(str, flag));


	ret = audio_info.rate != FLV_AUDIO_SOUNDRATE(flag);
	ERROR_RETURN(ret, "test_bit_struct:rate not match audio_info.rate=%d  FLV_AUDIO_SOUNDRATE=%d  bit=%s", audio_info.rate, FLV_AUDIO_SOUNDRATE(flag), bit_string(str, flag));

	ret = audio_info.audio_format != FLV_AUDIO_SOUNDFORMAT(flag);
	ERROR_RETURN(ret, "test_bit_struct:rate not match audio_info.rate=%d  FLV_AUDIO_SOUNDFORMAT=%d  bit=%s", audio_info.audio_format, FLV_AUDIO_SOUNDFORMAT(flag), bit_string(str, flag));

	return 0;
}


// test the bit struct in FLV_VIDEO_INFO and FLV_AUDIO_INFO
int test_bit_struct(int argc, const char *argv[])
{
	int verbose = (argc>2  && 0==strncmp(argv[2], "-v", 2));
	int ret;
	uint8_t flag = 0x72; // 01110010
	char str[10];  // 8 bits enough


	flag = 0x72;
	ret = test_audio_info(flag);
	ERROR_NEG_RETURN(-abs(ret), "test_bit_struct:test_audio_info flag=0x%x", flag);
	if (verbose) INFO_PRINT(1, "test_bit_struct:audio_info flag=0x%x bit=%s PASSED OK", flag, bit_string(str, flag));

	flag = 0xff;
	ret = test_audio_info(flag);
	ERROR_NEG_RETURN(-abs(ret), "test_bit_struct:test_audio_info flag=0x%x", flag);
	if (verbose) INFO_PRINT(2, "test_bit_struct:audio_info flag=0x%x bit=%s PASSED OK", flag, bit_string(str, flag));

	flag = 0x0;
	ret = test_audio_info(flag);
	ERROR_NEG_RETURN(-abs(ret), "test_bit_struct:test_audio_info flag=0x%x", flag);
	if (verbose) INFO_PRINT(3, "test_bit_struct:audio_info flag=0x%x bit=%s PASSED OK", flag, bit_string(str, flag));

	return 0;
}


// remember to skip the $program and "-test" in argv[0] and argv[1]
int test_all(int argc, const char *argv[])
{
	int ret;
	ret = test_uint24(argc, argv);
	ERROR_NEG_RETURN(ret, "test_uint24");

	
	ret = test_bit_struct(argc, argv);
	ERROR_NEG_RETURN(ret, "test_bit_struct");


	printf("test_all PASSED OK\n");
	return 0;
}


#ifndef FLV_LIB
// usage: flv [filename.flv] [epoch_tag]
// give a large epoch e.g. 1000 for all frames
int main(int argc, const char *argv[])
{
	const char * filename = "good.flv";
	int epoch = 10;  // read only first 20 tags (chunk)

	if (argc > 1 && 0==strcmp(argv[1], "-test")) {
		return test_all(argc, argv);
	}

	if (argc > 1) {
		filename = argv[1];
	}

	if (argc > 2) {
		epoch = atoi( argv[2]);
	}

	printf("read filename=%s\n", filename);
	read_flv(filename, epoch);


	return 0;
}
#endif
